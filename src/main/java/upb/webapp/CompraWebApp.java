package upb.webapp;

import upb.entity.Compra;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/compra")
public class CompraWebApp {

    @POST
    @Path("/post")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public javax.ws.rs.core.Response createTrackInJSON(Compra compra) {
        String result = "Compra Guardada : " + compra;
        Database b = new Database();
        Compra c = b.createC(compra.getIDCompra(), compra.getIDProductos(), compra.getCantidad());
        return javax.ws.rs.core.Response
                .status(200)
                .entity(c)
                .build();

    }


    @PUT
    @Path("/PUT/{IDCompra}")
    public javax.ws.rs.core.Response modificarCompra(@PathParam("IDCompra") String IDCompra, Compra compra) {
        Database b = new Database();
        b.modificarCompra(IDCompra, compra);
        return javax.ws.rs.core.Response
                .status(200)
                .build();

    }





    public  static void main(String [] args) {
        new Database().deleteP("YS123YasdaSY");

        new Database().closeDataBase();
    }
}

