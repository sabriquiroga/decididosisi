
package upb.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "compra")
public class Compra implements Serializable {

    @Id
    @Column(name = "IDCompra")
    private String IDCompra;

    @Column(name = "IDProductos")
    private String IDProductos;

    @Column(name = "Cantidad")
    private String Cantidad;

    public String getIDCompra() {
        return IDCompra;
    }

    public void setIDCompra(String IDCompra) {
        this.IDCompra = IDCompra;
    }

    public String getIDProductos() {
        return IDProductos;
    }

    public void setIDProductos(String IDProductos) {
        this.IDProductos = IDProductos;
    }

    public String getCantidad() {

        return Cantidad;
    }

    public void setCantidad(String Cantidad)
    {

        this.Cantidad = Cantidad;
    }



    @Override
    public String toString() {
        return IDCompra + "\t" + IDProductos + "\t" + Cantidad;
    }
}